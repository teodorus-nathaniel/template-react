import React from 'react'
import { useGetCategoryTree } from '../../../services/api/queries/global'
import Quotes from '../components/Quotes'

export default function HomeContainer () {
  const { data: categoryData } = useGetCategoryTree({})
  return (
    <Quotes data={categoryData} />
  )
}
