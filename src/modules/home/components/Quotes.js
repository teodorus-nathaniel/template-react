import React from 'react'
import bounceColor from '../../../static/images/loading-ruparupa.gif'

export default function Quotes ({ data }) {
  console.log('Category data Get', data)
  return (
    <div className='container-fluid'>
      <div className='row align-content-center justify-content-center' style={{ height: '100vh', backgroundColor: '#f9fafc' }}>
        <div className='col'>
          <center>
            <img src={bounceColor} width='100px' alt='loading' />
            <div className='fs-xxl'><b>Greetings The Chosen One!!</b></div>
            <br />
            <p className='fs-l'>
              This Project is already compatible with using <a href='https://reactjs.org/docs/hooks-intro.html' rel='react-hooks-documentation'><b><u>`React Hooks`</u></b></a>, [Make sure to learn about it first, before continuing].<br />
              Don't forget to check on the <b>README</b> file for some notes and instructions.<br />
              You can change anything inside this project to adjust to your project requirements. <br />
              The feedback will help us to improve standardization in our projects in the future.
            </p>
            <p className='fs-m'>“It’s what you do right now that makes a difference.”<br />
              &#8212; <i>Mark Bowden</i>
            </p>
          </center>
        </div>
      </div>

    </div>
  )
}
