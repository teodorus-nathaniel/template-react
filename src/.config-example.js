const config = {
    baseURL: 'http://localhost:3000',
    assetsURL: 'http://localhost:3000/assets',
    apiURL: 'https://v5.wrapper-sellercenter.ruparupastg.my.id',  // change with the project API WRAPPER
    imageURL: 'https://res.cloudinary.com/ruparupa-com/image/upload',
    googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAN_MGKh_1pLi6O1wXFRFBphZKB3Vdz4YU&v=3.37&libraries=geometry,drawing,places'
  }
  
  export default config
  