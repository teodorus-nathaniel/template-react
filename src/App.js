import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

// All pages
import Home from './pages/Home'

function App () {
  return (
    <Switch>
      {/* Add the router here */}
      <Route path='/home' component={Home} />
      <Redirect from='/' to='/home' />
    </Switch>
  )
}

export default App
