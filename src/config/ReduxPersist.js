// import immutablePersistenceTransform, { reducedStateHydrate } from '../Services/ImmutablePersistenceTransform'
import storage from 'redux-persist/lib/storage'

const REDUX_PERSIST = {
  active: true,
  reducerVersion: '1',
  storeConfig: {
    key: 'projectName', // changes it to your requirement
    storage
    // blacklist: [], // reducer keys that you do NOT want stored to persistence here
    // whitelist: ['auth'], // Optionally, just specify the keys you DO want stored to
    // persistence. An empty array means 'don't store any reducers' -> infinitered/ignite#409
    // transforms: [immutablePersistenceTransform],
    // redux-persist ^5 need implement manual reconciler to make immutable state work
    // stateReconciler: reducedStateHydrate
  }
}

export default REDUX_PERSIST
