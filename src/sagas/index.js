import { fork, all } from 'redux-saga/effects'
import API from '../services/GlobalApi'

import { login } from './UserSagas'

// ACTIVE THIS IF YOU NEED TOKEN HEADER
// import GET_TOKEN from '../Services/GetToken'

const api = API.create()
// const token = GET_TOKEN.getToken

function * clientSagas () {
  yield all([
    fork(login, api)
    // ACTIVE THIS IF YOU NEED TOKEN HEADER
    // fork(getUserRole, api, token)
  ])
}

export default function * rootSaga () {
  yield all([fork(clientSagas)])
}
