import { put, fork, take, call } from 'redux-saga/effects'
import { END } from 'redux-saga'
import UserActions, { UserTypes } from '../redux/UserRedux'
import isEmpty from 'lodash/isEmpty'
import localforage from 'localforage'

// EXAMPLE SAGAS
export function * login (api) {
  let action = yield take(UserTypes.USER_REQUEST)
  while (action !== END) {
    yield fork(fetchLogin, api, action)
    action = yield take(UserTypes.USER_REQUEST)
  }
}

export function * fetchLogin (api, { data }) {
  try {
    const res = yield call(api.login, data)
    if (!res.ok) {
      yield put(UserActions.userFailure())
    } else {
      if (isEmpty(res.data.errors)) {
        const data = res.data.data
        const date = new Date()
        if (data.token) {
          // set the access token for token
          yield localforage.setItem('access_token', data.token)
          // set the access date for expired
          yield localforage.setItem('access_date', date)
          if (data.user) {
            // for more detail data of the supplier
            if (data.user.supplier_id) {
              // using the supplier id from the user data
              const supplierId = { supplier_id: data.user.supplier_id }
              const resSupplier = yield call(api.getSupplierInfo, supplierId)
              if (!res.ok) {
                yield put(UserActions.userFailure())
              } else {
                if (isEmpty(resSupplier.data.errors)) {
                  const dataSupplier = resSupplier.data.data
                  if (!isEmpty(dataSupplier)) {
                    yield put(UserActions.supplierSuccess(dataSupplier))
                  }
                } else {
                  if (!isEmpty(res.data.errors.message)) {
                    yield put(UserActions.userFailure(res.data.errors.message))
                  } else {
                    yield put(UserActions.userFailure('Terjadi kesalahan, ulangi beberapa saat lagi.'))
                  }
                }
              }
            }
            // send the data to redux
            yield put(UserActions.userSuccess(data.user))
          }
        }
      } else {
        if (!isEmpty(res.data.errors.message)) {
          yield put(UserActions.userFailure(res.data.errors.message))
        } else {
          yield put(UserActions.userFailure('Terjadi kesalahan, ulangi beberapa saat lagi.'))
        }
      }
    }
  } catch (error) {
    console.log(error)
    yield put(UserActions.userFailure())
  }
}
