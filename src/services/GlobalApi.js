import config from '../config'
// ACTIVE THIS IF YOU NEED TOKEN HEADER
// import localforage from 'localforage'
import apisauce from 'apisauce'

const apiURL = config.apiURL

const create = () => {
  const apiWrapper = apisauce.create({
    baseURL: apiURL,
    timeout: 20000
  })

  const header = () => {
    return {
      headers: {
        'Content-Type': 'application/json'
        // 'Access-Control-Allow-Origin': 'http://local.sellercenter.ruparupa.io'
      }
    }
  }

  // ACTIVE THIS IF YOU NEED TOKEN HEADER
  //   const headerWithToken = (Authorization) => {
  //     return {
  //       headers: { ...header, Authorization: `Bearer ${Authorization}` }
  //     }
  //   }

  // ACTIVE THIS IF YOU NEED TOKEN HEADER
  // const token = window.localStorage.getItem('access_token')
  //   const tokenForage = async () => {
  //     let token = ''
  //     try {
  //       token = await localforage.getItem('access_token')
  //     } catch (error) {
  //     }
  //     return token
  //   }

  const login = (data) => {
    return apiWrapper.post('/auth/login', data, header())
  }

  const getCategory = () => {
    return apiWrapper.get('/category/tree/', {}, header())
  }

  const getBannerSearchBar = () => {
    return apiWrapper.get('/misc/search-banner')
  }

  const newsletterSubscribe = (data) => {
    return apiWrapper.post('/newsletter/subscribe', { data })
  }

  // how to use token as header example
  //   const changeSupplierInfo = async (data) => {
  //     const token = await tokenForage()
  //     return apiWrapper.post('/supplier/info', data, headerWithToken(token))
  //   }

  return {
    login,
    getCategory,
    getBannerSearchBar,
    newsletterSubscribe
  }
}

export default {
  create
}
