
import { isEmpty, startCase } from 'lodash'
import API from './Api'
import { notifyError, notifySuccess } from './Notify'

const api = API.create()

const message = { name: '', success: '', error: 'Terjadi kesalahan, ulangi beberapa saat lagi' }

const handleErrorMessage = (message, setErr) => {
  if (setErr !== null) {
    setErr({ err: true, message: message })
  }
  notifyError(message)
}

// THIS IS ONLY TEMPLATE
const fetchingGet = async (params, setRes, setIsLoading, setErr, api, name, successMessage) => {
  if (setIsLoading !== null) setIsLoading(true)
  try {
    const response = await (api)
    if (!response.ok) {
      const errMsg = (response.data?.message || 'Terjadi kesalahan, ulangi beberapa saat lagi ')
      if (setErr !== null)setErr({ err: true, message: errMsg })
    } else {
      if (response.status === 200) {
        if (!isEmpty(response.data?.errors)) {
          if (setErr !== null)setErr({ err: true, message: response.data.errors.message })
        } else {
          setRes(response.data.data)
          if (setErr !== null)setErr({ err: false, message: '' })
        }
      } else {
        const errMsg = `Error response status: ${response.status}, while trying to ${name}`
        if (setErr !== null)setErr({ err: true, message: errMsg })
      }
    }
  } catch (error) {
    const errMsg = 'Terjadi kesalahan, ulangi beberapa saat lagi '
    if (setErr !== null)setErr({ err: true, message: errMsg })
  }
  if (setIsLoading !== null) setIsLoading(false)
}

const fetchingPostPut = async (body, setIsLoading, setErr, setSuccess, api, message) => {
  if (setIsLoading !== null) setIsLoading(true)
  try {
    const response = await (api)
    if (!response.ok) {
      handleErrorMessage(message.error, setErr)
    } else {
      if (response.status === 200) {
        if (!isEmpty(response.data?.errors)) {
          handleErrorMessage(response.data.errors.message, setErr)
        } else {
          if (setErr !== null) {
            setErr({ err: false, message: '' })
          }

          if (message.success !== '') {
            notifySuccess(startCase(message.success))
          }
          if (setSuccess !== null) {
            setSuccess({ success: true, data: response.data.data })
          }
        }
      } else {
        message.error = `Error response status: ${response.status}, while trying to ${message.name}`
        handleErrorMessage(message.error, setErr)
      }
    }
  } catch (error) {
    handleErrorMessage(message.error, setErr)
  }
  if (setIsLoading !== null) setIsLoading(false)
}

const fetchingPostSomething = async (body, setIsLoading, setErr, setSuccess) => {
  message.name = 'fetchingPostSomething'
  message.success = ''
  fetchingPostPut(body, setIsLoading, setErr, setSuccess, api.fetchingPostSomething(body), message)
}

const fetchingGetSomething = async (params, setRes, setIsLoading, setErr) => {
  fetchingGet(params, setRes, setIsLoading, setErr, api.fetchingGetSomething(params), 'fetchSellerList')
}

// Add other fetching api here!
export {
  fetchingPostSomething,
  fetchingGetSomething
}
