import { globalAPI } from '../../IndexApi'
import { mutationWrapper } from '../base'

export const useSubscribeNewsLetter = mutationWrapper(globalAPI.newsletterSubscribe)
