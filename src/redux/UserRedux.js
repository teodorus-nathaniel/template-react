import { createActions, createReducer } from 'reduxsauce'
import Immutable from 'seamless-immutable'

// EVERYTHING HERE IS EXAMPLES, IT CAN CHANGES WITH YOUR PROJECT REQUIEREMENTS
const { Types, Creators } = createActions({
  userRequest: ['data'],
  userSuccess: ['data'],
  userFailure: ['err'],
  userLogout: null,
  userInit: null
})

export const UserTypes = Types
export default Creators

// Initial State
export const INITIAL_STATE = Immutable({
  err: null,
  fetching: false,
  success: false,
  user: null
})

export const init = (state) => {
  return ({
    ...state,
    fetching: false,
    err: null,
    user: null,
    success: false
  })
}

export const request = (state) => {
  return ({
    ...state,
    success: false,
    fetching: true
  })
}

export const success = (state, { data }) => {
  return ({
    ...state,
    success: true,
    fetching: false,
    user: data,
    err: null
  })
}

export const failure = (state, { err }) => {
  return ({
    ...state,
    success: false,
    fetching: false,
    err
  })
}

export const logout = (state) => {
  return ({ ...state, fetching: false })
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.USER_REQUEST]: request,
  [Types.USER_SUCCESS]: success,
  [Types.USER_FAILURE]: failure,
  [Types.USER_LOGOUT]: logout,
  [Types.USER_INIT]: init
})
