import React from 'react'
import HomeContainer from '../modules/home/containers/HomeContainer'
import { useGetCategoryTree } from '../services/api/queries/global'

// import { useDispatch, useSelector } from 'react-redux'

export default function Home () {
  const { data: categoryData } = useGetCategoryTree({})
  console.log('🚀 ~ file: Home.js ~ line 9 ~ Home ~ categoryData', categoryData)
  //
  // ACTIVE THIS IF YOU WANT TO CALL THE REDUX STORE. DON"T FORGET TO IMPORT IT
  // const user = useSelector(state => state.user)

  // ACTIVE THIS IF YOU WANT TO CALL THE REDUX. DON"T FORGET TO IMPORT IT
  // const dispatch = useDispatch()
  // dispatch(UserActions.userInit())

  return (
    <HomeContainer />
  )
}
